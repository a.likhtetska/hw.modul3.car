package vehicles;

import details.Engine;
import professions.Driver;

public class SportCar extends Car{
    int speed;

    public SportCar (String nameCar, String classCar, int weightCar, Driver driver, Engine engine, int speed) {
        super(nameCar, classCar, weightCar, driver, engine);
        this.speed = speed;
    }

    @Override
    public void start() {
        super.start();
    }

    @Override
    public void stop() {
        super.stop();
    }

    @Override
    public void turnRight() {
        super.turnRight();
    }

    @Override
    public void turnLeft() {
        super.turnLeft();
    }
}
