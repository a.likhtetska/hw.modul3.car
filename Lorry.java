package vehicles;

import details.Engine;
import professions.Driver;

public class Lorry extends Car {
    int lift;

    public Lorry( String nameCar, String classCar, int weightCar, Driver driver, Engine engine, int lift) {
        super(nameCar, classCar, weightCar, driver, engine);
        this.lift = lift;

    }

            @Override
    public void start() {
        super.start();
    }

    @Override
    public void stop() {
        super.stop();
    }

    @Override
    public void turnRight() {
        super.turnRight();
    }

    @Override
    public void turnLeft() {
        super.turnLeft();
    }
}
